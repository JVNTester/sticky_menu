function CloneMenu(obj){
	this.parent_elem = obj.parent;
	this.offset_menu = this.parent_elem.offsetTop;
	var clone = this;
	window.onscroll = function(){
		var scrolled = window.pageYOffset;
		var find_clone_menu = document.querySelector('.clone_menu');
		if(scrolled > clone.offset_menu){
			if(find_clone_menu == null){
				var new_menu = clone.parent_elem.cloneNode(true);
				new_menu.style.position = 'fixed';
				new_menu.style.top = '0px';
				new_menu.style.left = '0px';
				new_menu.style.opacity = '0.8';
				new_menu.classList.add('clone_menu');
				document.body.appendChild(new_menu);
			}
		}else{
			find_clone_menu.parentNode.removeChild(find_clone_menu);
		}
	}
}